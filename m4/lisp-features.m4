dnl c.f. make-config.sh

# SET_LISP_FEATURES([$lisp_os], [$lisp_cpu])
# ------------------------------------------
# Use $host_os and $host_cpu to determine which features to support
# AC_SUBST([LISP_FEATURES])
AC_DEFUN([SET_LISP_FEATURES],
[
# CPU class
LISP_FEATURES=":$2"


# OS family
case $1 in
windows)
	LISP_FEATURES="$LISP_FEATURES :win32"
	;;
*)
	LISP_FEATURES="$LISP_FEATURES :unix"
	;;
esac


# Executable format
case $1 in
darwin | windows)
	;;
*)
	LISP_FEATURES="$LISP_FEATURES :elf"
	;;
esac


# OS details
case $1 in
*bsd)
	LISP_FEATURES="$LISP_FEATURES :bsd :$1"
	if test $1 = freebsd -a $2 = x86
	then
		LISP_FEATURES="$LISP_FEATURES :restore-tls-segment-register-from-context"
	fi
	;;
darwin)
	LISP_FEATURES="$LISP_FEATURES :mach-o :bsd :darwin"
	case $2 in
	x86)
		LISP_FEATURES="$LISP_FEATURES :mach-exception-handler :sb-lutex :restore-fs-segment-register-from-tls"
		;;
	x86_64)
		LISP_FEATURES="$LISP_FEATURES :mach-exception-handler :sb-lutex"
		;;
	*)
		;;
	esac
	;;
linux)
	LISP_FEATURES="$LISP_FEATURES :linux"
	case $2 in
	x86 | mips)
		LISP_FEATURES="$LISP_FEATURES :largefile"
		;;
	*)
		;;
	esac
	;;
osf1)
	LISP_FEATURES="$LISP_FEATURES :osf1"
	;;
sunos)
	LISP_FEATURES="$LISP_FEATURES :sunos"
	if test $2 = x86
	then
		LISP_FEATURES="$LISP_FEATURES :sb-lutex"
	fi
	;;
windows)
	;;
esac


# Garbage collector
case $2 in
x86 | x86_64 | ppc)
	LISP_FEATURES="$LISP_FEATURES :gencgc"
	;;
*)
	;;
esac


# Call stack
case $2 in
x86 | x86_64)
	LISP_FEATURES="$LISP_FEATURES :stack-grows-downward-not-upward :c-stack-is-control-stack"
	;;
*)
	;;
esac


# VOPs
case $2 in
x86)
	LISP_FEATURES="$LISP_FEATURES :compare-and-swap-vops :unwind-to-frame-and-call-vop"
	;;
x86_64)
	LISP_FEATURES="$LISP_FEATURES :compare-and-swap-vops :unwind-to-frame-and-call-vop"
	;;
esac


# Is this truly universal?
LISP_FEATURES="$LISP_FEATURES :stack-allocatable-closures"


# Alien callbacks
case $1-$2 in
*-x86 | *-x86_64 | *-mips | darwin-ppc)
	LISP_FEATURES="$LISP_FEATURES :alien-callbacks"
	;;
*)
	;;
esac


# Linkage table
case $2 in
alpha)
	;;
*)
	LISP_FEATURES="$LISP_FEATURES :linkage-table"
	;;
esac


# Dynamic loading support
if test $HAVE_DLOPEN != no
then
	LISP_FEATURES="$LISP_FEATURES :os-provides-dlopen"
fi
# dlshim hack
case $1-$2 in
windows-x86 | darwin-ppc)
	LISP_FEATURES="$LISP_FEATURES :os-provides-dlopen"
	;;
*)
	;;
esac


# Reverse lookup
if test $HAVE_DLADDR = yes
then
	LISP_FEATURES="$LISP_FEATURES :os-provides-dladdr"
fi


# Widechar support
if test $HAVE_PUTWC = yes
then
	LISP_FEATURES="$LISP_FEATURES :os-provides-putwc"
fi


# Block sizes
if test $GROVELIT_BLKSIZE_T_FOUND = yes
then
	LISP_FEATURES="$LISP_FEATURES :os-provides-blksize-t"
fi


# Microsecond support
if test $GROVELIT_SUSECONDS_T_FOUND = yes
then
	LISP_FEATURES="$LISP_FEATURES :os-provides-suseconds-t"
fi


AC_SUBST(LISP_FEATURES)


# Stack size hack
case $1-$2 in
darwin-ppc)
    # The default stack ulimit under darwin is too small to run PURIFY.
    # Best we can do is complain and exit at this stage
    if [ "`ulimit -s`" = "512" ]; then
        echo "Your stack size limit is too small to build SBCL."
        echo "See the limit(1) or ulimit(1) commands and the README file."
        exit 1
    fi
	;;
*)
	;;
esac
])
