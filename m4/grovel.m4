# GROVELD([define], [headers], [value-if-not-found])
# --------------------------------------------------
# Check for the C preprocessor expansion of $define
#
# Approximates AC_SUBST(AS_TR_CPP(GROVELD_$define), macroexpand($define))
#
# Patterned after AC_EGREP_CPP
AC_DEFUN([GROVELD],
[AC_MSG_CHECKING([for $1])
AC_COMPUTE_INT(AS_TR_CPP([GROVELD_$1]),
	[$1], [$2],
	[AS_TR_CPP([GROVELD_$1])=$3])
AC_MSG_RESULT($AS_TR_CPP([GROVELD_$1]))
AC_SUBST(AS_TR_CPP([GROVELD_$1]))
])



# GROVELIT([type], [headers])
# ---------------------------
# Check for the integer characteristics of $type
#
# AC_SUBST:
#   GROVELIT_$TYPE_FOUND={yes,no}
#   GROVELIT_$TYPE_POLARITY={signed,unsigned}
#   GROVELIT_$TYPE_BYTES=sizeof(type)
#   GROVELIT_$TYPE_BITS=8*sizeof(type)
# where $TYPE = AS_TR_CPP($type)
#
# If FOUND=no, the other variables will be empty.
#
AC_DEFUN([GROVELIT],
[grovelit_found=no
grovelit_polarity=
grovelit_bytes=
grovelit_bits=
AC_COMPILE_IFELSE([m4_default([$2], [$ac_includes_default])
$1 x;],
	[grovelit_found=yes
AC_COMPILE_IFELSE(
	[AC_LANG_BOOL_COMPILE_TRY(
		[m4_default([$2], [$ac_includes_default])],
		[(($1) -1) < 0])],
	[grovelit_polarity=signed],
	[grovelit_polarity=unsigned])
dnl
AC_CHECK_SIZEOF([$1], [], [m4_default([$2], [$ac_includes_default])])
grovelit_bytes=$AS_TR_SH([ac_cv_sizeof_$1])
grovelit_bits=$((8 * $grovelit_bytes))
dnl
	],
	[])
if test $grovelit_found != no
then
	echo "result: $1 is $grovelit_polarity-$grovelit_bits"
fi
AC_SUBST(AS_TR_CPP([GROVELIT_$1_FOUND]), [$grovelit_found])
AC_SUBST(AS_TR_CPP([GROVELIT_$1_POLARITY]), [$grovelit_polarity])
AC_SUBST(AS_TR_CPP([GROVELIT_$1_BYTES]), [$grovelit_bytes])
AC_SUBST(AS_TR_CPP([GROVELIT_$1_BITS]), [$grovelit_bits])
])

