dnl Replacement for make-config.sh


# LISP_CONFIG()
# -------------
#
# Detect optional support for the host platform
AC_DEFUN([LISP_CONFIG],
[# Do we need to bring in undefineds.c?
port_incomplete=no


## OS
case $host_os in
freebsd*)
	os=bsd
	;;
netbsd* | openbsd*)
	os=bsd
	port_incomplete=yes
	;;

darwin*)
	os=darwin
	;;

linux*)
	os=linux
	;;

osf1*)
	os=osf1
	port_incomplete=yes
	;;

sunos* | solaris*)
	os=sunos
	;;

cygwin* | mingw* | win*)
	os=windows
	port_incomplete=yes
	;;

*)
	AC_MSG_ERROR([unknown OS $host_os])
	;;
esac
AM_CONDITIONAL([COND_OS_BSD], [test $os = bsd])
AM_CONDITIONAL([COND_OS_DARWIN], [test $os = darwin])
AM_CONDITIONAL([COND_OS_LINUX], [test $os = linux])
AM_CONDITIONAL([COND_OS_OSF1], [test $os = osf1])
AM_CONDITIONAL([COND_OS_SUNOS], [test $os = sunos])
AM_CONDITIONAL([COND_OS_WINDOWS], [test $os = windows])



## CPU
case $host_cpu in
alpha*)
	cpu=alpha
	;;
hppa)
	cpu=hppa
	port_incomplete=yes
	;;
mips)
	cpu=mips
	;;
powerpc* | ppc*)
	cpu=ppc
	;;
sparc)
	cpu=sparc
	#port_incomplete=yes
	;;
i?86)
	cpu=x86
	;;
x86_64 | amd64)
	cpu=x86_64
	;;
*)
	AC_MSG_ERROR([unknown CPU $host_cpu])
	;;
esac
AC_SUBST(LISP_CPU, [$cpu])
AM_CONDITIONAL([COND_CPU_ALPHA], [test $cpu = alpha])
AM_CONDITIONAL([COND_CPU_HPPA], [test $cpu = hppa])
AM_CONDITIONAL([COND_CPU_MIPS], [test $cpu = mips])
AM_CONDITIONAL([COND_CPU_PPC], [test $cpu = ppc])
AM_CONDITIONAL([COND_CPU_SPARC], [test $cpu = sparc])
AM_CONDITIONAL([COND_CPU_X86], [test $cpu = x86])
AM_CONDITIONAL([COND_CPU_X86_64], [test $cpu = x86_64])

AM_CONDITIONAL([COND_PORT_INCOMPLETE], [test $port_incomplete = yes])
])
