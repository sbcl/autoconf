# FIND_LISP()
# -----------
# Auto-detect a lisp to use on the build system
#
# AC_SUBST(BUILD_LISP, $found)
# AC_SUBST(COMPILE_LISP, ...) -- a command suitable for compiling lisp files
#
dnl Use BUILD_LISP instead of LISP, to circumvent a naive check in automake that
dnl catches common errors (grep your automake script for "is an anachronism").
dnl Version 1.9 has hacks that allow JAVA and PYTHON (but not LISP).
AC_DEFUN([FIND_LISP],
[AC_ARG_WITH([lisp],
	AS_HELP_STRING([--with-lisp=path], [Name of or absolute path to the lisp \
which should bootstrap SBCL. @<:@default=autodetect@:>@]),
	[case $withval in
	yes) find_lisp= ;; # autodetect
	no) AC_MSG_ERROR([don't use --with-lisp=no or --without-lisp; \
SBCL bootstraps from a Lisp on the build system]) ;;
	*) find_lisp=$withval ;;
	esac],
	[find_lisp=])
if test x$find_lisp = x
then
	AC_CHECK_PROGS(BUILD_LISP, [sbcl cmucl lisp clisp], no)
else
	AC_CHECK_PROG(BUILD_LISP, $find_lisp, $find_lisp, no)
fi
case $BUILD_LISP in
no)
	AC_MSG_ERROR([could not find a Lisp for bootstrapping])
	;;
sbcl)
	COMPILE_LISP="sbcl --userinit /dev/null --sysinit /dev/null"
	;;
cmucl)
	COMPILE_LISP="cmucl -batch -noinit"
	;;
lisp)
	# Assuming cmucl
	COMPILE_LISP="lisp -batch -noinit"
	;;
clisp)
	AC_MSG_ERROR([do not know how to use clisp])
	;;
esac
AC_SUBST(BUILD_LISP)
AC_SUBST(COMPILE_LISP)
])

