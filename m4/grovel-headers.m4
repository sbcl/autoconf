dnl Replacement for tools-for-build/grovel-headers.c

# GROVEL_HEADERS([grovel-headers.h])
#
AC_DEFUN([GROVEL_HEADERS],
[## Build up a list of system headers to grovel
echo "Searching for system headers"
headers="stdio.h stdlib.h sys/types.h sys/stat.h fcntl.h unistd.h signal.h errno.h"
extraheaders=
AC_CHECK_HEADERS([windows.h shlobj.h sys/times.h sys/wait.h sys/ioctl.h sys/termios.h],
[headers="$headers $ac_header"])
AC_CHECK_HEADER([dlfcn.h],
[headers="$headers dlfcn.h"],
[extraheaders="$extraheaders src/runtime/darwin-dlshim.h"])
AC_CHECK_HEADER([langinfo.h],
[headers="$headers langinfo.h"],
[extraheaders="$extraheaders src/runtime/darwin-langinfo.h"])

echo > $1
for h in $headers; do echo "#include <$h>" >> $1; done
if test x$extraheaders != x
then
	for h in $extraheaders; do echo "#include \"$h\"" >> $1; done
fi
])

AC_DEFUN([GROVEL_VALUES],
[#### Find #defines
echo "Grovelling for C integer values"


### Windows
#    defconstant ("MAX_PATH", MAX_PATH);
GROVELDS(MAX_PATH)
#    defconstant ("CSIDL_DESKTOP", CSIDL_DESKTOP);
GROVELDS(CSIDL_DESKTOP)
#    defconstant ("CSIDL_INTERNET", CSIDL_INTERNET);
GROVELDS(CSIDL_INTERNET)
#    defconstant ("CSIDL_PROGRAMS", CSIDL_PROGRAMS);
GROVELDS(CSIDL_PROGRAMS)
#    defconstant ("CSIDL_CONTROLS", CSIDL_CONTROLS);
GROVELDS(CSIDL_CONTROLS)
#    defconstant ("CSIDL_PRINTERS", CSIDL_PRINTERS);
GROVELDS(CSIDL_PRINTERS)
#    defconstant ("CSIDL_PERSONAL", CSIDL_PERSONAL);
GROVELDS(CSIDL_PERSONAL)
#    defconstant ("CSIDL_FAVORITES", CSIDL_FAVORITES);
GROVELDS(CSIDL_FAVORITES)
#    defconstant ("CSIDL_STARTUP", CSIDL_STARTUP);
GROVELDS(CSIDL_STARTUP)
#    defconstant ("CSIDL_RECENT", CSIDL_RECENT);
GROVELDS(CSIDL_RECENT)
#    defconstant ("CSIDL_SENDTO", CSIDL_SENDTO);
GROVELDS(CSIDL_SENDTO)
#    defconstant ("CSIDL_BITBUCKET", CSIDL_BITBUCKET);
GROVELDS(CSIDL_BITBUCKET)
#    defconstant ("CSIDL_STARTMENU", CSIDL_STARTMENU);
GROVELDS(CSIDL_STARTMENU)
#    defconstant ("CSIDL_DESKTOPDIRECTORY", CSIDL_DESKTOPDIRECTORY);
GROVELDS(CSIDL_DESKTOPDIRECTORY)
#    defconstant ("CSIDL_DRIVES", CSIDL_DRIVES);
GROVELDS(CSIDL_DRIVES)
#    defconstant ("CSIDL_NETWORK", CSIDL_NETWORK);
GROVELDS(CSIDL_NETWORK)
#    defconstant ("CSIDL_NETHOOD", CSIDL_NETHOOD);
GROVELDS(CSIDL_NETHOOD)
#    defconstant ("CSIDL_FONTS", CSIDL_FONTS);
GROVELDS(CSIDL_FONTS)
#    defconstant ("CSIDL_TEMPLATES", CSIDL_TEMPLATES);
GROVELDS(CSIDL_TEMPLATES)
#    defconstant ("CSIDL_COMMON_STARTMENU", CSIDL_COMMON_STARTMENU);
GROVELDS(CSIDL_COMMON_STARTMENU)
#    defconstant ("CSIDL_COMMON_PROGRAMS", CSIDL_COMMON_PROGRAMS);
GROVELDS(CSIDL_COMMON_PROGRAMS)
#    defconstant ("CSIDL_COMMON_STARTUP", CSIDL_COMMON_STARTUP);
GROVELDS(CSIDL_COMMON_STARTUP)
#    defconstant ("CSIDL_COMMON_DESKTOPDIRECTORY", CSIDL_COMMON_DESKTOPDIRECTORY);
GROVELDS(CSIDL_COMMON_DESKTOPDIRECTORY)
#    defconstant ("CSIDL_APPDATA", CSIDL_APPDATA);
GROVELDS(CSIDL_APPDATA)
#    defconstant ("CSIDL_PRINTHOOD", CSIDL_PRINTHOOD);
GROVELDS(CSIDL_PRINTHOOD)
#    defconstant ("CSIDL_LOCAL_APPDATA", CSIDL_LOCAL_APPDATA);
GROVELDS(CSIDL_LOCAL_APPDATA)
#    defconstant ("CSIDL_ALTSTARTUP", CSIDL_ALTSTARTUP);
GROVELDS(CSIDL_ALTSTARTUP)
#    defconstant ("CSIDL_COMMON_ALTSTARTUP", CSIDL_COMMON_ALTSTARTUP);
GROVELDS(CSIDL_COMMON_ALTSTARTUP)
#    defconstant ("CSIDL_COMMON_FAVORITES", CSIDL_COMMON_FAVORITES);
GROVELDS(CSIDL_COMMON_FAVORITES)
#    defconstant ("CSIDL_INTERNET_CACHE", CSIDL_INTERNET_CACHE);
GROVELDS(CSIDL_INTERNET_CACHE)
#    defconstant ("CSIDL_COOKIES", CSIDL_COOKIES);
GROVELDS(CSIDL_COOKIES)
#    defconstant ("CSIDL_HISTORY", CSIDL_HISTORY);
GROVELDS(CSIDL_HISTORY)
#    defconstant ("CSIDL_COMMON_APPDATA", CSIDL_COMMON_APPDATA);
GROVELDS(CSIDL_COMMON_APPDATA)
#    defconstant ("CSIDL_WINDOWS", CSIDL_WINDOWS);
GROVELDS(CSIDL_WINDOWS)
#    defconstant ("CSIDL_SYSTEM", CSIDL_SYSTEM);
GROVELDS(CSIDL_SYSTEM)
#    defconstant ("CSIDL_PROGRAM_FILES", CSIDL_PROGRAM_FILES);
GROVELDS(CSIDL_PROGRAM_FILES)
#    defconstant ("CSIDL_MYPICTURES", CSIDL_MYPICTURES);
GROVELDS(CSIDL_MYPICTURES)
#    defconstant ("CSIDL_PROFILE", CSIDL_PROFILE);
GROVELDS(CSIDL_PROFILE)
#    defconstant ("CSIDL_SYSTEMX86", CSIDL_SYSTEMX86);
GROVELDS(CSIDL_SYSTEMX86)
#    defconstant ("CSIDL_PROGRAM_FILESX86", CSIDL_PROGRAM_FILESX86);
GROVELDS(CSIDL_PROGRAM_FILESX86)
#    defconstant ("CSIDL_PROGRAM_FILES_COMMON", CSIDL_PROGRAM_FILES_COMMON);
GROVELDS(CSIDL_PROGRAM_FILES_COMMON)
#    defconstant ("CSIDL_PROGRAM_FILES_COMMONX86", CSIDL_PROGRAM_FILES_COMMONX86);
GROVELDS(CSIDL_PROGRAM_FILES_COMMONX86)
#    defconstant ("CSIDL_COMMON_TEMPLATES", CSIDL_COMMON_TEMPLATES);
GROVELDS(CSIDL_COMMON_TEMPLATES)
#    defconstant ("CSIDL_COMMON_DOCUMENTS", CSIDL_COMMON_DOCUMENTS);
GROVELDS(CSIDL_COMMON_DOCUMENTS)
#    defconstant ("CSIDL_COMMON_ADMINTOOLS", CSIDL_COMMON_ADMINTOOLS);
GROVELDS(CSIDL_COMMON_ADMINTOOLS)
#    defconstant ("CSIDL_ADMINTOOLS", CSIDL_ADMINTOOLS);
GROVELDS(CSIDL_ADMINTOOLS)
#    defconstant ("CSIDL_CONNECTIONS", CSIDL_CONNECTIONS);
GROVELDS(CSIDL_CONNECTIONS)
#    defconstant ("CSIDL_COMMON_MUSIC", CSIDL_COMMON_MUSIC);
GROVELDS(CSIDL_COMMON_MUSIC)
#    defconstant ("CSIDL_COMMON_PICTURES", CSIDL_COMMON_PICTURES);
GROVELDS(CSIDL_COMMON_PICTURES)
#    defconstant ("CSIDL_COMMON_VIDEO", CSIDL_COMMON_VIDEO);
GROVELDS(CSIDL_COMMON_VIDEO)
#    defconstant ("CSIDL_RESOURCES", CSIDL_RESOURCES);
GROVELDS(CSIDL_RESOURCES)
#    defconstant ("CSIDL_RESOURCES_LOCALIZED", CSIDL_RESOURCES_LOCALIZED);
GROVELDS(CSIDL_RESOURCES_LOCALIZED)
#    defconstant ("CSIDL_COMMON_OEM_LINKS", CSIDL_COMMON_OEM_LINKS);
GROVELDS(CSIDL_COMMON_OEM_LINKS)
#    defconstant ("CSIDL_CDBURN_AREA", CSIDL_CDBURN_AREA);
GROVELDS(CSIDL_CDBURN_AREA)
#    defconstant ("CSIDL_COMPUTERSNEARME", CSIDL_COMPUTERSNEARME);
GROVELDS(CSIDL_COMPUTERSNEARME)
#    defconstant ("CSIDL_FLAG_DONT_VERIFY", CSIDL_FLAG_DONT_VERIFY);
GROVELDS(CSIDL_FLAG_DONT_VERIFY)
#    defconstant ("CSIDL_FLAG_CREATE", CSIDL_FLAG_CREATE);
GROVELDS(CSIDL_FLAG_CREATE)
#    defconstant ("CSIDL_FLAG_MASK", CSIDL_FLAG_MASK);
GROVELDS(CSIDL_FLAG_MASK)

## Exception codes
#    defconstant("+exception-access-violation+", EXCEPTION_ACCESS_VIOLATION);
GROVELDS(EXCEPTION_ACCESS_VIOLATION)
#    defconstant("+exception-array-bounds-exceeded+", EXCEPTION_ARRAY_BOUNDS_EXCEEDED);
GROVELDS(EXCEPTION_ARRAY_BOUNDS_EXCEEDED)
#    defconstant("+exception-breakpoint+", EXCEPTION_BREAKPOINT);
GROVELDS(EXCEPTION_BREAKPOINT)
#    defconstant("+exception-datatype-misalignment+", EXCEPTION_DATATYPE_MISALIGNMENT);
GROVELDS(EXCEPTION_DATATYPE_MISALIGNMENT)
#    defconstant("+exception-flt-denormal-operand+", EXCEPTION_FLT_DENORMAL_OPERAND);
GROVELDS(EXCEPTION_FLT_DENORMAL_OPERAND)
#    defconstant("+exception-flt-divide-by-zero+", EXCEPTION_FLT_DIVIDE_BY_ZERO);
GROVELDS(EXCEPTION_FLT_DIVIDE_BY_ZERO)
#    defconstant("+exception-flt-inexact-result+", EXCEPTION_FLT_INEXACT_RESULT);
GROVELDS(EXCEPTION_FLT_INEXACT_RESUL)
#    defconstant("+exception-flt-invalid-operation+", EXCEPTION_FLT_INVALID_OPERATION);
GROVELDS(EXCEPTION_FLT_INVALID_OPERATION)
#    defconstant("+exception-flt-overflow+", EXCEPTION_FLT_OVERFLOW);
GROVELDS(EXCEPTION_FLT_OVERFLOW)
#    defconstant("+exception-flt-stack-check+", EXCEPTION_FLT_STACK_CHECK);
GROVELDS(EXCEPTION_FLT_STACK_CHECK)
#    defconstant("+exception-flt-underflow+", EXCEPTION_FLT_UNDERFLOW);
GROVELDS(EXCEPTION_FLT_UNDERFLOW)
#    defconstant("+exception-illegal-instruction+", EXCEPTION_ILLEGAL_INSTRUCTION);
GROVELDS(EXCEPTION_ILLEGAL_INSTRUCTION)
#    defconstant("+exception-in-page-error+", EXCEPTION_IN_PAGE_ERROR);
GROVELDS(EXCEPTION_IN_PAGE_ERROR)
#    defconstant("+exception-int-divide-by-zero+", EXCEPTION_INT_DIVIDE_BY_ZERO);
GROVELDS(EXCEPTION_INT_DIVIDE_BY_ZERO)
#    defconstant("+exception-int-overflow+", EXCEPTION_INT_OVERFLOW);
GROVELDS(EXCEPTION_INT_OVERFLOW)
#    defconstant("+exception-invalid-disposition+", EXCEPTION_INVALID_DISPOSITION);
GROVELDS(EXCEPTION_INVALID_DISPOSITION)
#    defconstant("+exception-noncontinuable-exception+", EXCEPTION_NONCONTINUABLE_EXCEPTION);
GROVELDS(EXCEPTION_NONCONTINUABLE_EXCEPTION)
#    defconstant("+exception-priv-instruction+", EXCEPTION_PRIV_INSTRUCTION);
GROVELDS(EXCEPTION_PRIV_INSTRUCTION)
#    defconstant("+exception-single-step+", EXCEPTION_SINGLE_STEP);
GROVELDS(EXCEPTION_SINGLE_STEP)
#    defconstant("+exception-stack-overflow+", EXCEPTION_STACK_OVERFLOW);
GROVELDS(EXCEPTION_STACK_OVERFLOW)


## FormatMessage
#    defconstant ("FORMAT_MESSAGE_ALLOCATE_BUFFER", FORMAT_MESSAGE_ALLOCATE_BUFFER);
GROVELDS(FORMAT_MESSAGE_ALLOCATE_BUFFER)
#    defconstant ("FORMAT_MESSAGE_FROM_SYSTEM", FORMAT_MESSAGE_FROM_SYSTEM);
GROVELDS(FORMAT_MESSAGE_FROM_SYSTEM)


## Errors
#    defconstant ("ERROR_ENVVAR_NOT_FOUND", ERROR_ENVVAR_NOT_FOUND);
GROVELDS(ERROR_ENVVAR_NOT_FOUND)

## GetComputerName
#    defconstant ("MAX_COMPUTERNAME_LENGTH", MAX_COMPUTERNAME_LENGTH);
GROVELDS(MAX_COMPUTERNAME_LENGTH)
#    defconstant ("ERROR_BUFFER_OVERFLOW", ERROR_BUFFER_OVERFLOW);
GROVELDS(ERROR_BUFFER_OVERFLOW)



### Unix
#    defconstant("o_rdonly", _O_RDONLY);
GROVELDS(_O_RDONLY)
#    defconstant("o_wronly", _O_WRONLY);
GROVELDS(_O_WRONLY)
#    defconstant("o_rdwr",   _O_RDWR);
GROVELDS(_O_RDWR)
#    defconstant("o_creat",  _O_CREAT);
GROVELDS(_O_CREAT)
#    defconstant("o_trunc",  _O_TRUNC);
GROVELDS(_O_TRUNC)
#    defconstant("o_append", _O_APPEND);
GROVELDS(_O_APPEND)
#    defconstant("o_excl",   _O_EXCL);
GROVELDS(_O_EXCL)
#    defconstant("o_binary", _O_BINARY);
GROVELDS(_O_BINARY)

#    defconstant("enoent", ENOENT);
GROVELDS(ENOENT)
#    defconstant("eexist", EEXIST);
GROVELDS(EEXIST)

#    defconstant("s-ifmt",  S_IFMT);
GROVELDS(S_IFMT)
#    defconstant("s-ifdir", S_IFDIR);
GROVELDS(S_IFDIR)
#    defconstant("s-ifreg", S_IFREG);
GROVELDS(S_IFREG)
#    defconstant ("rtld-lazy", RTLD_LAZY);
GROVELDS(RTLD_LAZY)
#    defconstant ("rtld-now", RTLD_NOW);
GROVELDS(RTLD_NOW)
#    defconstant ("rtld-global", RTLD_GLOBAL);
GROVELDS(RTLD_GLOBAL)
#    defconstant("codeset", CODESET);
GROVELDS(CODESET)
#    defconstant("r_ok", R_OK);
GROVELDS(R_OK)
#    defconstant("w_ok", W_OK);
GROVELDS(W_OK)
#    defconstant("x_ok", X_OK);
GROVELDS(X_OK)
#    defconstant("f_ok", F_OK);
GROVELDS(F_OK)
#    defconstant("o_rdonly",  O_RDONLY);
GROVELDS(O_RDONLY)
#    defconstant("o_wronly",  O_WRONLY);
GROVELDS(O_WRONLY)
#    defconstant("o_rdwr",    O_RDWR);
GROVELDS(O_RDWR)
#    defconstant("o_accmode", O_ACCMODE);
GROVELDS(O_ACCMODE)
#    defconstant("o_creat",   O_CREAT);
GROVELDS(O_CREAT)
#    defconstant("o_excl",    O_EXCL);
GROVELDS(O_EXCL)
#    defconstant("o_noctty",  O_NOCTTY);
GROVELDS(O_NOCTTY)
#    defconstant("o_trunc",   O_TRUNC);
GROVELDS(O_TRUNC)
#    defconstant("o_append",  O_APPEND);
GROVELDS(O_APPEND)


## LISP_FEATURE_LARGEFILE
#    defconstant("o_largefile", O_LARGEFILE);
GROVELDS(O_LARGEFILE)
#    defconstant("s-ifmt",  S_IFMT);
GROVELDS(S_IFMT)
#    defconstant("s-ififo", S_IFIFO);
GROVELDS(S_IFIFO)
#    defconstant("s-ifchr", S_IFCHR);
GROVELDS(S_IFCHR)
#    defconstant("s-ifdir", S_IFDIR);
GROVELDS(S_IFDIR)
#    defconstant("s-ifblk", S_IFBLK);
GROVELDS(S_IFBLK)
#    defconstant("s-ifreg", S_IFREG);
GROVELDS(S_IFREG)

#    defconstant("s-iflnk",  S_IFLNK);
GROVELDS(S_IFLNK)
#    defconstant("s-ifsock", S_IFSOCK);
GROVELDS(S_IFSOCK)


## error numbers
#    deferrno("enoent", ENOENT);
GROVELDS(ENOENT)
#    deferrno("eintr", EINTR);
GROVELDS(EINTR)
#    deferrno("eio", EIO);
GROVELDS(EIO)
#    deferrno("eexist", EEXIST);
GROVELDS(EEXIST)
#    deferrno("espipe", ESPIPE);
GROVELDS(ESPIPE)
#    deferrno("ewouldblock", EWOULDBLOCK);
GROVELDS(EWOULDBLOCK)


## for wait3(2) in run-program.lisp
#    defconstant("wnohang", WNOHANG);
GROVELDS(WNOHANG)
#    defconstant("wuntraced", WUNTRACED);
GROVELDS(WUNTRACED)


## various ioctl(2) flags
#    defconstant("tiocnotty",  TIOCNOTTY);
GROVELDS(TIOCNOTTY)
#    defconstant("tiocgwinsz", TIOCGWINSZ);
GROVELDS(TIOCGWINSZ)
#    defconstant("tiocswinsz", TIOCSWINSZ);
GROVELDS(TIOCSWINSZ)
#    defconstant("tiocgpgrp",  TIOCGPGRP);
GROVELDS(TIOCGPGRP)
#    defconstant("tiocspgrp",  TIOCSPGRP);
GROVELDS(TIOCSPGRP)
#    defconstant("sig-dfl", (unsigned long)SIG_DFL);
GROVELDS(SIG_DFL)
#    defconstant("sig-ign", (unsigned long)SIG_IGN);
GROVELDS(SIG_IGN)

#    defsignal("sigalrm", SIGALRM);
GROVELDS(SIGALRM)
#    defsignal("sigbus", SIGBUS);
GROVELDS(SIGBUS)
#    defsignal("sigchld", SIGCHLD);
GROVELDS(SIGCHLD)
#    defsignal("sigcont", SIGCONT);
GROVELDS(SIGCONT)
#    defsignal("sigemt", SIGEMT);
GROVELDS(SIGEMT)
#    defsignal("sigfpe", SIGFPE);
GROVELDS(SIGFPE)
#    defsignal("sighup", SIGHUP);
GROVELDS(SIGHUP)
#    defsignal("sigill", SIGILL);
GROVELDS(SIGILL)
#    defsignal("sigint", SIGINT);
GROVELDS(SIGINT)
#    defsignal("sigio", SIGIO);
GROVELDS(SIGIO)
#    defsignal("sigiot", SIGIOT);
GROVELDS(SIGIOT)
#    defsignal("sigkill", SIGKILL);
GROVELDS(SIGKILL)
#    defsignal("sigpipe", SIGPIPE);
GROVELDS(SIGPIPE)
#    defsignal("sigprof", SIGPROF);
GROVELDS(SIGPROF)
#    defsignal("sigquit", SIGQUIT);
GROVELDS(SIGQUIT)
#    defsignal("sigsegv", SIGSEGV);
GROVELDS(SIGSEGV)
#    defsignal("sigstkflt", SIGSTKFLT);
GROVELDS(SIGSTKFLT)
#    defsignal("sigstop", SIGSTOP);
GROVELDS(SIGSTOP)
#    defsignal("sigsys", SIGSYS);
GROVELDS(SIGSYS)
#    defsignal("sigterm", SIGTERM);
GROVELDS(SIGTERM)
#    defsignal("sigtrap", SIGTRAP);
GROVELDS(SIGTRAP)
#    defsignal("sigtstp", SIGTSTP);
GROVELDS(SIGTSTP)
#    defsignal("sigttin", SIGTTIN);
GROVELDS(SIGTTIN)
#    defsignal("sigttou", SIGTTOU);
GROVELDS(SIGTTOU)
#    defsignal("sigurg", SIGURG);
GROVELDS(SIGURG)
#    defsignal("sigusr1", SIGUSR1);
GROVELDS(SIGUSR1)
#    defsignal("sigusr2", SIGUSR2);
GROVELDS(SIGUSR2)
#    defsignal("sigvtalrm", SIGVTALRM);
GROVELDS(SIGVTALRM)
#    defsignal("sigwaiting", SIGWAITING);
GROVELDS(SIGWAITING)
#    defsignal("sigwinch", SIGWINCH);
GROVELDS(SIGWINCH)
#    defsignal("sigxcpu", SIGXCPU);
GROVELDS(SIGXCPU)
#    defsignal("sigxfsz", SIGXFSZ);
GROVELDS(SIGXFSZ)

# Floating point exception codes. Some of these are missing on Darwin.
#    defconstant("fpe-intovf", FPE_INTOVF);
GROVELDS(FPE_INTOVF, -1)
#    defconstant("fpe-intdiv", FPE_INTDIV);
GROVELDS(FPE_INTDIV, -1)
#    defconstant("fpe-fltdiv", FPE_FLTDIV);
GROVELDS(FPE_FLTDIV)
#    defconstant("fpe-fltovf", FPE_FLTOVF);
GROVELDS(FPE_FLTOVF)
#    defconstant("fpe-fltund", FPE_FLTUND);
GROVELDS(FPE_FLTUND)
#    defconstant("fpe-fltres", FPE_FLTRES);
GROVELDS(FPE_FLTRES)
#    defconstant("fpe-fltinv", FPE_FLTINV);
GROVELDS(FPE_FLTINV)
#    defconstant("fpe-fltsub", FPE_FLTSUB);
GROVELDS(FPE_FLTSUB, -1)




#### Find types
echo "Grovelling for C types"
#    defconstant ("input-record-size", sizeof (INPUT_RECORD));
GROVELITS(INPUT_RECORD)
#    DEFTYPE("int-ptr", INT_PTR);
GROVELITS(INT_PTR)
#    DEFTYPE("dword",   DWORD);
GROVELITS(DWORD)
#    DEFTYPE("bool",    BOOL);
GROVELITS(BOOL)
#    DEFTYPE("uint",    UINT);
GROVELITS(UINT)
#    DEFTYPE("ulong",   ULONG);
GROVELITS(ULONG)
#    DEFTYPE("ino-t",  ino_t);
GROVELITS(ino_t)
#    DEFTYPE("time-t", time_t);
GROVELITS(time_t)
#    DEFTYPE("off-t",  off_t);
GROVELITS(off_t)
#    DEFTYPE("size-t", size_t);
GROVELITS(size_t)
#    DEFTYPE("mode-t", mode_t);
GROVELITS(mode_t)
#    DEFTYPE("wst-dev-t", wst_dev_t);
GROVELITS(wst_dev_t)
#    DEFTYPE("wst-off-t", wst_off_t);
GROVELITS(wst_off_t)
#    DEFTYPE("wst-blksize-t", wst_blksize_t);
GROVELITS(wst_blksize_t)
#    DEFTYPE("wst-blkcnt-t", wst_blkcnt_t);
GROVELITS(wst_blkcnt_t)
#    DEFTYPE("wst-nlink-t", wst_nlink_t);
GROVELITS(wst_nlink_t)
#    DEFTYPE("wst-uid-t", wst_uid_t);
GROVELITS(wst_uid_t)
#    DEFTYPE("wst-gid-t", wst_gid_t);
GROVELITS(wst_gid_t)
#    DEFTYPE("clock-t", clock_t);
GROVELITS(clock_t)
#    DEFTYPE("dev-t",   dev_t);
GROVELITS(dev_t)
#    DEFTYPE("gid-t",   gid_t);
GROVELITS(gid_t)
#    DEFTYPE("nlink-t", nlink_t);
GROVELITS(nlink_t)
#    DEFTYPE("suseconds-t", long);
GROVELITS(long)
#    DEFTYPE("suseconds-t", suseconds_t);
GROVELITS(suseconds_t)
#    DEFTYPE("uid-t",   uid_t);
GROVELITS(uid_t)
# tools-for-build/os-provides-blksize-t-test.c
GROVELITS(blksize_t)
echo "Done grovelling"
])

