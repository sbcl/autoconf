dnl c.f. make-config.sh

# REMOVE_DIR_SAFELY([dir], [os])
# ------------------------------
AC_DEFUN([REMOVE_DIR_SAFELY],
[if test $2 = windows
then
	if test -d "$1"
	then
		rm -rf "$1"
	elif test -e "$1"
	then
		AC_MSG_ERROR([afraid to 'rm -rf' non-directory "$1"])
	fi
else
	if test -h $1
	then
		rm $1
	elif test -w $1
	then
		AC_MSG_ERROR([afraid to replace non-symlink $1 with a symlink])
	fi
fi
])

dnl
dnl LN_S was meant for use in makefiles...
dnl not configure scripts (where it doesn't always work)
dnl



# LINK_FILES([[$lisp_os], [$lisp_cpu])
# ------------------------------------
# Bleh: "Configure" SBCL by linking files to known locations.
# Todo: Use conditional makefile sources and header files with #ifdef.
AC_DEFUN([LINK_FILES],
[echo
echo "*****************************"
echo "Making configuration symlinks"
echo "*****************************"
original_dir=`pwd`

for d in src/compiler src/assembly; do
    echo //setting up symlink $d/target
    REMOVE_DIR_SAFELY([$d/target], [$1])
    cd ./$d
    if test -d $2
	then
        $LN_S $2 target
    else
        echo "missing sbcl_arch directory $PWD/$2"
        exit 1
    fi
    cd "$original_dir"
done


echo //setting up symlink src/compiler/assembly
REMOVE_DIR_SAFELY([src/compiler/assembly], [$1])
cd src/compiler
$LN_S ../assembly assembly

cd "$original_dir"
cd ./src/runtime/
rm -f Config target-arch-os.h target-arch.h target-os.h target-lispregs.h

# Config
case $1 in
windows)
	$LN_S Config.$2-win32 Config
	;;
*)
	$LN_S Config.$2-$1 Config
	;;
esac

# target-arch-os.h
case $1 in
*bsd)
	$LN_S $2-bsd-os.h target-arch-os.h
	;;
windows)
	$LN_S $2-win32-os.h target-arch-os.h
	;;
*)
	$LN_S $2-$1-os.h target-arch-os.h
	;;
esac

# target-arch.h
$LN_S $2-arch.h target-arch.h

# target-os.h
case $1 in
*bsd | darwin)
	$LN_S bsd-os.h target-os.h
	;;
windows)
	$LN_S win32-os.h target-os.h
	;;
*)
	$LN_S $1-os.h target-os.h
	;;
esac

# target-lispregs.h
$LN_S $2-lispregs.h target-lispregs.h


cd "$original_dir"


# Make a unique ID for this build (to discourage people from
# mismatching sbcl and *.core files).
mkdir -p output
if test `uname` = "SunOS"
then
  # use /usr/xpg4/bin/id instead of /usr/bin/id
  PATH=/usr/xpg4/bin:$PATH
fi
echo '"'`hostname`-`id -un`-`date +%Y-%m-%d-%H-%M-%S`'"' > output/build-id.tmp
])

