#!/bin/bash

echo "SBCL has been installed to $INSTALL_ROOT."
echo "Please make sure that $INSTALL_ROOT/bin is in your system PATH."
if test $INSTALL_ROOT != /usr/local; then
echo "To use, export SBCL_HOME=$INSTALL_ROOT/lib/sbcl or call
sbcl --core $INSTALL_ROOT/lib/sbcl/sbcl.core"
fi

