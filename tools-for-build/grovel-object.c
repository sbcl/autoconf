/*
	Grovel a compiled object file for an integer constant.

	Example source file:
	#include <inttypes.h>
	#include <signal.h>
	uint64_t values[]={0x0123456789abcdefLL, FPE_INTDIV, 0xfedcba9876543210LL};
	int dummy[]={5, 6, 7, 8};
	void test() { char *s = (char *)values; s = (char *)dummy; }
*/

#include <stdio.h>
#include <inttypes.h>

// search for a known pattern in the file
int match(char *buf, FILE *f)
{
  int count=0;
  char c;
  while(fread(&c, 1, 1, f))
    {
      if(c==buf[count])
	{
	  count++;
	}
      else
	{
	  count=0;
	}
      if(count==8)
	return 1;
    }
  return 0;
}

int main(int argc, char **argv)
{
  if(argc!=3 || !(argv[1][0]=='0' || argv[1][0]=='1'))
    {
      printf("argc: %d\n", argc);
      printf("argv[2]: %s\n", argv[2]);
      // 0 = little-endian
      // 1 = big-endian
      printf("Usage: %s {0,1} <input-file>\n", argv[0]);
      return 1;
    }

  FILE *f=fopen(argv[2], "r");
  if(!f)
    {
      printf("Error: could not open '%s'", argv[1]);
      return 2;
    }

  // constants can be up to 64 bits = 8 bytes long
  // format:  0x01234567xxxxxxxx76543210
  int count=0;
  char buf;
  // count up
  uint64_t value=0;
  if(argv[1][0]=='0')
    {
      // little endian
      unsigned char prefix[]={0xef, 0xcd, 0xab, 0x89, 0x67, 0x45, 0x23, 0x01};
      unsigned char suffix[]={0x10, 0x32, 0x54, 0x76, 0x98, 0xba, 0xdc, 0xfe};
      if(!match(prefix, f))
	return 3;
      unsigned char buffer[8];
      if(fread(buffer, 8, 1, f)!=1)
	return 4;
      if(!match(suffix, f))
	return 5;

      // decode the value
      int i;
      for(i=0; i<8; i++)
	{
	  value*=8;
	  value+=buffer[7-i];
	}
    }
  else
    {
      // big endian
      unsigned char prefix[]={0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef};
      unsigned char suffix[]={0xfe, 0xdc, 0xba, 0x98, 0x76, 0x54, 0x32, 0x10};
      if(!match(prefix, f))
	return 6;
      unsigned char buffer[8];
      if(fread(buffer, 8, 1, f)!=1)
	return 7;
      if(!match(suffix, f))
	return 8;

      // decode the value
      int i;
      for(i=0; i<8; i++)
	{
	  value*=8;
	  value+=buffer[i];
	}
    }

  if(fclose(f))
    return 9;

  printf("%llu\n", value);
}
